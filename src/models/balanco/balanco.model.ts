export class Liquido {

  id: number;
  volume: number;
  liquido: string;

  constructor(liquido: any){
    this.id = new Date().getTime();
    this.volume = liquido.volume;
    this.liquido = liquido.liquido;
  }

}


export class Balanco {

  id: number;
  liquidosAdministrado: Liquido[];
  liquidosExpelido: Liquido[];
  perdasInsensiveis: number;
  data: Date;

  constructor(balanco: any){
    this.id = new Date().getTime();
    this.liquidosAdministrado = balanco.liquidosAdministrado;
    this.liquidosExpelido = balanco.liquidosExpelido;
    this.perdasInsensiveis = balanco.perdasInsensiveis;
    this.data = new Date();
  }

}
