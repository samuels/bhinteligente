import { Balanco } from '../balanco/balanco.model';
import { DateTime } from "ionic-angular";

export class Paciente {

  id: number;
  nome: string;
  balancos: Balanco[] = [];
  peso: number;
  dataHoraInternacao: DateTime;

  constructor(paciente: any){
    this.id = new Date().getTime();
    this.nome = paciente.nome;
    this.peso = paciente.peso;
    this.dataHoraInternacao = paciente.dataHoraInternacao;
  }

}
