import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalancoAddLePage } from './balanco-add-le';

@NgModule({
  declarations: [
    BalancoAddLePage,
  ],
  imports: [
    IonicPageModule.forChild(BalancoAddLePage),
  ],
})
export class BalancoAddLePageModule {}
