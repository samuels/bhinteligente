import { Component } from '@angular/core';

import { IonicPage, NavController } from 'ionic-angular';

import { LiquidosAdministradosPage } from '../liquidos-administrados/liquidos-administrados';
import { LiquidosEliminadosPage } from '../liquidos-eliminados/liquidos-eliminados';
import { BhPage } from '../bh/bh';
import { PerdasInsesiveisPage } from '../perdas-insesiveis/perdas-insesiveis';


import { Provider } from '../../providers/provider/provider';


@IonicPage()
@Component({
  selector: 'page-balanco-dia',
  templateUrl: 'balanco-dia.html',
})
export class BalancoDiaPage {

  totalLA: number;
  totalLE: number;
  bh: number;
  PI: number;

  constructor(public navCtrl: NavController, private provider: Provider) {
  }

  ionViewCanEnter() {
    this.getBh();
    this.getTotalLA();
    this.getTotalLE();
    this.getCalcPI();
  }

  liqAdm(){
    this.navCtrl.push(LiquidosAdministradosPage)
  }

  liqEli(){
    this.navCtrl.push(LiquidosEliminadosPage)
  }

  perdasInsensiveis(){
    this.navCtrl.push(PerdasInsesiveisPage)
  }

  viewBh(){
    this.navCtrl.push(BhPage)
  }

  private getBh(){
    this.provider.bh()
      .then(valor => {
        this.bh = valor
      });
  }

  private getTotalLA(){
    this.provider.total({'data': 'BHD', 'value': 'LA'})
      .then(total => {
        this.totalLA = total;
      });
  }

  private getTotalLE(){
    this.provider.total({'data': 'BHD', 'value': 'LE'})
      .then(total => {
        this.totalLE = total
      })
  }

  private getCalcPI(){
    this.provider.calcPI()
      .then(valor => {
        this.PI = valor
      })
  }

}
