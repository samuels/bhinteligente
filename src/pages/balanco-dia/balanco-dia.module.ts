import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalancoDiaPage } from './balanco-dia';

@NgModule({
  declarations: [
    BalancoDiaPage,
  ],
  imports: [
    IonicPageModule.forChild(BalancoDiaPage),
  ],
  exports: [
    BalancoDiaPage
  ]
})
export class BalancoDiaPageModule {}
