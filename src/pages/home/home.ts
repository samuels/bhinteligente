import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { BalancoDiaPage } from '../balanco-dia/balanco-dia';
import { BalancoConsolidarPage } from '../balanco-consolidar/balanco-consolidar';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  pages: Array<{title: string, component: any}>;

  constructor(public navCtrl: NavController) {
    this.pages = [
      {title: 'Balanço Hídrico Diário', component: BalancoDiaPage},
      {title: 'Consolidar Balanço Hídrico', component: BalancoConsolidarPage}
    ]
  }

  openPage(page){
    this.navCtrl.push(page.component)
  }

}
