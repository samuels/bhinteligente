import { Component } from '@angular/core';
import {IonicPage, ViewController} from 'ionic-angular';

import {Validators, FormBuilder, FormGroup} from '@angular/forms';
import {Paciente} from "../../models/paciente/paciente.model";

@IonicPage()
@Component({
  selector: 'page-paciente-create',
  templateUrl: 'paciente_create.html',
})
export class PacienteCreatePage {

  form: FormGroup;

  constructor(private viewCtrl: ViewController, private formBuilder: FormBuilder) {
    this.createForm();
  }

  private createForm(){
    this.form = this.formBuilder.group({
      nome: ['', Validators.compose([])],
      peso: ['', Validators.compose([])],
      dataHoraInternacao: ['', Validators.compose([])],
    })
  }

  close(){
    this.viewCtrl.dismiss();
  }

  save(){
    if(this.form.valid){
      let paciente = new Paciente(this.form.value);
      this.viewCtrl.dismiss(paciente);
    }
  }

}
