import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalancoConsolidarPage } from './balanco-consolidar';

import { ModalCbhPageModule } from './modal-cbh/modal-cbh.module';

import { Ng2OrderModule } from 'ng2-order-pipe';

@NgModule({
  declarations: [
    BalancoConsolidarPage,
  ],
  imports: [
    IonicPageModule.forChild(BalancoConsolidarPage),
    ModalCbhPageModule,
    Ng2OrderModule
  ],
  exports: [
    BalancoConsolidarPage
  ]
})
export class BalancoConsolidarPageModule {}
