import { Component } from '@angular/core';
import { IonicPage, ModalController, AlertController } from 'ionic-angular';

import { Provider } from '../../providers/provider/provider';
import { CreatePdfProvider } from '../../providers/create-pdf/create-pdf';

import { ModalCbhPage } from './modal-cbh/modal-cbh';

@IonicPage()
@Component({
  selector: 'page-balanco-consolidar',
  templateUrl: 'balanco-consolidar.html',
})
export class BalancoConsolidarPage {

  listBalancos: any = [];
  total: number = 0;

  constructor(private provider: Provider, public modalCtrl: ModalController, public alertCtrl: AlertController, private createPdf: CreatePdfProvider) {
    this.getCBH();
    this.getTotalCBH();
  }

  private getCBH(){
    this.provider.get({'data': 'CBH'})
      .then(data => {
        this.listBalancos = data;
      })
  }

  private getTotalCBH(){
    this.provider.totalCBH()
      .then(valor => {
        this.total = valor;
      })
  }

  addModal(){
    let modal = this.modalCtrl.create(ModalCbhPage);
    modal.onDidDismiss(data => {
      if(data){
        this.provider.set({'data': 'CBH'}, data)
          .then(() => {
            this.getCBH();
            this.getTotalCBH();
          })
      }
    });
    return modal.present();
  }

  apagar(balanco){
    let index = this.listBalancos.indexOf(balanco);
    this.provider.remove({'data': 'CBH'}, index)
      .then(() => {
        this.getCBH();
        this.getTotalCBH();
      })
  }

  resertInfor(){
    this.provider.initCBH()
      .then(()=>{
        this.getCBH();
        this.getTotalCBH();
      })
  }

  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Paciente',
      message: "Digite o nome do paciente!",
      inputs: [
        {
          name: 'Nome',
          placeholder: 'Nome'
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: value => {
            if(value['Nome']){
              let data = {
                paciente: value['Nome'],
                CBHtotal: this.total,
                CBHlist: this.listBalancos
              };
              this.createPdf.compartilhar({'local': 'CBH', 'data': data})
            }else{
              this.showPrompt();
            }
          }
        }
      ]
    });
    prompt.present();
  }

}
