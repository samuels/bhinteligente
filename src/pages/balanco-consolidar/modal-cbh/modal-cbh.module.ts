import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalCbhPage } from './modal-cbh';

@NgModule({
  declarations: [
    ModalCbhPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalCbhPage),
  ],
  exports: [
    ModalCbhPage
  ]
})
export class ModalCbhPageModule {}
