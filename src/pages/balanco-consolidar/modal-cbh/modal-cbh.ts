import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-modal-cbh',
  templateUrl: './modal-cbh.html',
})
export class ModalCbhPage {

  valor = '';

  data: any = {'value': '', 'date': ''};

  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController) {
  }

  digito(dig){
    if(((dig == '+' || dig == '-') && this.data['value'].indexOf(dig) > -1) || ((dig == '+' || dig == '-') && this.data['value'] != '')){
      this.data['value'] = this.data['value'];
    }else if(dig == '.' && this.data['value'] == ''){
      this.data['value'] = '0.'
    }else if (dig == '.' && this.data['value'].indexOf(dig) > -1){
      this.data['value'] = this.data['value'];
    }else {
      this.data['value'] += dig;
    }
  }

  apagar(){
    if (this.data['value'].length > 0){
      this.data['value'] = this.data['value'].substring(0,this.data['value'].length - 1);
    }
  }

  ok(){
    if(this.data['value'] && this.data['date']){
      this.viewCtrl.dismiss(this.data);
    }else {
      if(!this.data['value']){
        this.presentToast('Insira o valor do balanço!');
      }else if(!this.data['date']){
        this.presentToast('Insira a data!');
      }
    }
  }

  cancelar(){
    this.viewCtrl.dismiss();
  }

  private presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

}
