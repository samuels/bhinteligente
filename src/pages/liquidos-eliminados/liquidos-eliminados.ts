import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import { ModalController } from 'ionic-angular';

import { LiquidosEliminadosModalPage } from './liquidos-eliminados-modal/liquidos-eliminados-modal';

import { Provider } from '../../providers/provider/provider';

@IonicPage()
@Component({
  selector: 'page-liquidos-eliminados',
  templateUrl: 'liquidos-eliminados.html',
})
export class LiquidosEliminadosPage {

  liquidos: Array<{slug: string, verbose: string}>;
  liqData: any = {};
  listLiquidos: any = [];
  total: number = 0;

  constructor(public modalCtrl: ModalController, private provider: Provider) {
    this.getliquidos();
    this.getTotal();
    this.liquidos = [
      {
        slug: 'vomito',
        verbose: 'Vômito'
      },
      {
        slug: 'urina',
        verbose: 'Urina'
      },
      {
        slug: 'diarreia',
        verbose: 'Diarréia'
      },
      {
        slug: 'liquido-sonda',
        verbose: 'Liquidos por Sonda'
      },
      {
        slug: 'drenos-geral',
        verbose: 'Drenos em geral'
      },
      {
        slug: 'ostomia',
        verbose: 'Ostomia'
      },
      {
        slug: 'outros',
        verbose: 'Outros'
      }
    ];
  }

  private getliquidos(){
    this.provider.get({'data': 'BHD', 'value': 'LE'})
      .then(data => {
        this.listLiquidos = data;
      })
  }

  getTotal(){
    this.provider.total({'data': 'BHD', 'value': 'LE'})
      .then(valor=>{
        this.total = valor;
      })
  }

  selectLiquido(liquido){
    this.liqData['liquido'] = this.liquidos.filter(x => x.slug == liquido)[0];
    this.createModal();
  }

  private createModal(){
    let modal = this.modalCtrl.create(LiquidosEliminadosModalPage);
    modal.onDidDismiss(valor => {
      if(valor){
        this.liqData['volume'] = valor;
        this.provider.set({'data': 'BHD', 'value': 'LE'}, this.liqData)
          .then(() => {
            this.getliquidos();
            this.getTotal();
          });
      }
    });
    return modal.present();
  }

  apagar(liquido){
    let index = this.listLiquidos.indexOf(liquido);
    this.provider.remove({'data': 'BHD', 'value': 'LE'}, index)
      .then(() => {
        this.getliquidos();
        this.getTotal();
      })
  }
}
