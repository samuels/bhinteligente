import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiquidosEliminadosPage } from './liquidos-eliminados';

import { LiquidosEliminadosModalModule } from './liquidos-eliminados-modal/liquidos-eliminados-modal.module';

import { Ng2OrderModule } from 'ng2-order-pipe';

@NgModule({
  declarations: [
    LiquidosEliminadosPage,
  ],
  imports: [
    IonicPageModule.forChild(LiquidosEliminadosPage),
    LiquidosEliminadosModalModule,
    Ng2OrderModule
  ],
  exports: [
    LiquidosEliminadosPage
  ]
})
export class LiquidosEliminadosModule {}
