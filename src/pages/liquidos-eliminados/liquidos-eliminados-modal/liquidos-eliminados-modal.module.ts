import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiquidosEliminadosModalPage } from './liquidos-eliminados-modal';

@NgModule({
  declarations: [
    LiquidosEliminadosModalPage,
  ],
  imports: [
    IonicPageModule.forChild(LiquidosEliminadosModalPage),
  ],
  exports: [
    LiquidosEliminadosModalPage
  ]
})
export class LiquidosEliminadosModalModule {}
