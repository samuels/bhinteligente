import { Component } from '@angular/core';
import { IonicPage, ToastController, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'liquidos-eliminados-modal',
  templateUrl: 'liquidos-eliminados-modal.html',
})
export class LiquidosEliminadosModalPage {

  valor: string = '';

  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController) {

  }

  digito(dig){
    if(dig == '.' && this.valor == ''){
      this.valor = '0.'
    }else if (dig == '.' && this.valor.indexOf(dig) > -1){
      this.valor = this.valor;
    }else {
      this.valor += dig;
    }
  }

  apagar(){
    if (this.valor.length > 0){
      this.valor = this.valor.substring(0,this.valor.length - 1);
    }
  }

  ok(){
    if(this.valor){
      let valor = parseFloat(this.valor);
      this.viewCtrl.dismiss(valor);
    }else {
      this.presentToast();
    }
  }

  cancelar(){
    this.viewCtrl.dismiss();
  }

  private presentToast() {
    let toast = this.toastCtrl.create({
      message: 'Digite o volume!',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}
