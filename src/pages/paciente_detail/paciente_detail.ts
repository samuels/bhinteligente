import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { PacienteProvider } from '../../providers/paciente/paciente.service';
import { Paciente } from "../../models/paciente/paciente.model";

@IonicPage()
@Component({
  selector: 'page-paciente-detail',
  templateUrl: 'paciente_detail.html',
})
export class PacienteDetailPage {

  paciente: Paciente;

  constructor(private pacienteProvider: PacienteProvider) {
    this.paciente = this.pacienteProvider.pacienteAberto;
  }

  newBalanco(){

  }

}
