import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiquidosAdministradosPage } from './liquidos-administrados';

import { LiquidosAdministradosModalModule } from './liquidos-administrados-modal/liquidos-administrados-modal.module';

import { Ng2OrderModule } from 'ng2-order-pipe';

@NgModule({
  declarations: [
    LiquidosAdministradosPage,
  ],
  imports: [
    IonicPageModule.forChild(LiquidosAdministradosPage),
    LiquidosAdministradosModalModule,
    Ng2OrderModule,
  ],
  exports: [
    LiquidosAdministradosPage
  ]
})
export class LiquidosAdministradosModule {}
