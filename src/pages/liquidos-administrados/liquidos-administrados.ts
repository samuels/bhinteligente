import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';

import { ModalController } from 'ionic-angular';

import { LiquidosAdministradosModalPage } from './liquidos-administrados-modal/liquidos-administrados-modal';

import { Provider } from '../../providers/provider/provider';

@IonicPage()
@Component({
  selector: 'page-liquidos-administrados',
  templateUrl: 'liquidos-administrados.html',
})
export class LiquidosAdministradosPage {


  liquidos: Array<{slug: string, verbose: string}>;
  liqData: any = {};
  listLiquidos: any = [];
  total: number = 0;

  constructor(public modalCtrl: ModalController, private provider: Provider) {
    this.getliquidos();
    this.getTotal();
    this.liquidos = [
      {
        slug: 'npt',
        verbose: 'NPT - Nutrição Parenteral'
      },
      {
        slug: 'hemocomponentes',
        verbose: 'Hemocomponentes'
      },
      {
        slug: 'plano-soro',
        verbose: 'Plano de Soro'
      },
      {
        slug: 'medicamentos',
        verbose: 'Medicamentos'
      },
      {
        slug: 'sonda',
        verbose: 'Sonda'
      },
      {
        slug: 'liquido-oral',
        verbose: 'Liquido via oral'
      },
      {
        slug: 'outros',
        verbose: 'Outros'
      }
    ];
  }

  private getliquidos(){
    this.provider.get({'data': 'BHD', 'value': 'LA'})
      .then(data => {
        this.listLiquidos = data;
      })
  }

  getTotal(){
    this.provider.total({'data': 'BHD', 'value': 'LA'})
      .then(valor=>{
        this.total = valor;
      })
  }

  selectLiquido(liquido){
    this.liqData['liquido'] = this.liquidos.filter(x => x.slug == liquido)[0];
    this.createModal()
  }

  private createModal(){
    let modal = this.modalCtrl.create(LiquidosAdministradosModalPage);
    modal.onDidDismiss(valor => {
      if(valor){
        this.liqData['volume'] = valor;
        this.provider.set({'data': 'BHD', 'value': 'LA'}, this.liqData)
          .then(() => {
            this.getliquidos();
            this.getTotal();
          });
      }
    });
    return modal.present();
  }

  apagar(liquido){
    let index = this.listLiquidos.indexOf(liquido);
    this.provider.remove({'data': 'BHD', 'value': 'LA'}, index)
      .then(() => {
        this.getliquidos();
        this.getTotal();
      })
  }

}
