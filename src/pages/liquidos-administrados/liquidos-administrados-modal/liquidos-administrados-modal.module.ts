import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiquidosAdministradosModalPage } from './liquidos-administrados-modal';

@NgModule({
  declarations: [
    LiquidosAdministradosModalPage,
  ],
  imports: [
    IonicPageModule.forChild(LiquidosAdministradosModalPage),
  ],
  exports: [
    LiquidosAdministradosModalPage
  ]
})
export class LiquidosAdministradosModalModule {}
