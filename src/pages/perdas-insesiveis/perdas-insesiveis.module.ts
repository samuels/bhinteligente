import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PerdasInsesiveisPage } from './perdas-insesiveis';

import { ModalPageModule } from './modal/modal.module'

@NgModule({
  declarations: [
    PerdasInsesiveisPage,
  ],
  imports: [
    IonicPageModule.forChild(PerdasInsesiveisPage),
    ModalPageModule
  ],
  exports: [
    PerdasInsesiveisPage
  ]
})
export class PerdasInsesiveisPageModule {}
