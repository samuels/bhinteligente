import { Component } from '@angular/core';
import { IonicPage, ModalController } from 'ionic-angular';

import { ModalPage } from './modal/modal';

import { Provider } from '../../providers/provider/provider';

@IonicPage()
@Component({
  selector: 'page-perdas-insesiveis',
  templateUrl: 'perdas-insesiveis.html',
})
export class PerdasInsesiveisPage {

  peso: number;
  horasInternacao: string;
  calcPI: number;

  constructor(private provider: Provider, public modalCtrl: ModalController) {
    this.getPI();
    this.calc();
  }

  getPI(){
    this.provider.get({'data': 'BHD', 'value': 'PI'})
      .then(data => {
        this.peso = data['peso'];
        this.horasInternacao = data['horas'];
      })
  }

  calc(){
    this.provider.calcPI()
      .then(valor => {
        this.calcPI = valor;
      })
  }

  openModal(){
    let modal = this.modalCtrl.create(ModalPage);
    modal.onDidDismiss( data => {
      if(data){
        this.provider.set({'data': 'BHD', 'value': 'PI'}, data)
          .then(() => {
            this.getPI();
            this.calc();
          })
      }
    });
    return modal.present();
  }

}
