import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController } from 'ionic-angular';

import { Provider } from '../../../providers/provider/provider';

@IonicPage()
@Component({
  selector: 'page-modal',
  templateUrl: './modal.html',
})
export class ModalPage {

  peso: string;
  horas: string;


  constructor(public viewCtrl: ViewController, public toastCtrl: ToastController, private provider: Provider) {
    this.getPI();
  }

  getPI(){
    this.provider.get({'data': 'BHD', 'value': 'PI'})
      .then(data => {
        this.peso = data['peso'].toString();
        this.horas = data['horas'];
      })
  }

  digito(dig){
    if(dig == '.' && this.peso == ''){
      this.peso = '0.'
    }else if (dig == '.' && this.peso.indexOf(dig) > -1){
      this.peso = this.peso;
    }else {
      this.peso += dig;
    }
  }

  apagar(){
    if (this.peso.length > 0){
      this.peso = this.peso.substring(0,this.peso.length - 1);
    }
  }

  cancelar(){
    this.viewCtrl.dismiss();
  }

  ok(){
    let data = {};
    if(this.peso){
      data['peso'] = parseFloat(this.peso);
    }else {
      this.presentToast('Digite o Peso!');
    }
    if(this.horas){
      data['horas'] = this.horas;
    }else{
      this.presentToast('Selecione o numero de horas!')
    }
    this.viewCtrl.dismiss(data);
  }

  private presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }


}
