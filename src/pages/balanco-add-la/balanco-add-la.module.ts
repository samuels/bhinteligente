import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalancoAddLaPage } from './balanco-add-la';

@NgModule({
  declarations: [
    BalancoAddLaPage,
  ],
  imports: [
    IonicPageModule.forChild(BalancoAddLaPage),
  ],
})
export class BalancoAddLaPageModule {}
