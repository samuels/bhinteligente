import { Component } from '@angular/core';
import { IonicPage, AlertController} from 'ionic-angular';

import { Provider } from '../../providers/provider/provider';
import { CreatePdfProvider } from '../../providers/create-pdf/create-pdf';

@IonicPage()
@Component({
  selector: 'page-bh',
  templateUrl: 'bh.html',
})
export class BhPage {

  bh: number;
  totalLA: number;
  LA: any = [];
  totalLE: number;
  LE: any = [];
  PI: number;
  peso: number;
  numHoras: string;

  constructor(private provider: Provider, private createPdf: CreatePdfProvider, public alertCtrl: AlertController) {
  }

  ionViewCanEnter() {
    this.initView()
  }
  
  private initView(){
    this.getBh();
    this.getTotalLA();
    this.getLA();
    this.getTotalLE();
    this.getLE();
    this.calcPI();
    this.getInfoPI();
  }

  private getBh(){
    this.provider.bh()
      .then(valor => {
        this.bh = valor
      });
  }

  private getTotalLA(){
    this.provider.total({'data': 'BHD', 'value': 'LA'})
      .then(total => {
        this.totalLA = total;
      });
  }

  private getLA(){
    this.provider.get({'data': 'BHD', 'value': 'LA'})
      .then(data => {
        this.LA =data;
      })
  }

  private getTotalLE(){
    this.provider.total({'data': 'BHD', 'value': 'LE'})
      .then(total => {
        this.totalLE = total
      })
  }

  private getLE(){
    this.provider.get({'data': 'BHD', 'value': 'LE'})
      .then(data => {
        this.LE = data;
      })
  }

  private calcPI(){
    this.provider.calcPI()
      .then(valor => {
        this.PI = valor;
      })
  }

  private getInfoPI(){
    this.provider.get({'data': 'BHD', 'value': 'PI'})
      .then(data => {
        this.peso = data['peso'];
        this.numHoras = data['horas'];
      })
  }

  resertInfor(){
    this.provider.initBHD()
      .then(() => {
        this.initView();
      })
  }


  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Paciente',
      message: "Digite o nome do paciente!",
      inputs: [
        {
          name: 'Nome',
          placeholder: 'Nome'
        },
      ],
      buttons: [
        {
          text: 'Cancelar'
        },
        {
          text: 'Confirmar',
          handler: value => {
            if(value['Nome']){
              let data = {
                paciente: value['Nome'],
                peso: this.peso,
                numHoras: this.numHoras,
                LA: this.LA,
                totalLA: this.totalLA,
                LE: this.LE,
                totalLE: this.totalLE,
                bh: this.bh,
                PI: this.PI
              };
              this.createPdf.compartilhar({'local': 'BHD', 'data': data})
            }else{
              this.showPrompt();
            }
          }
        }
      ]
    });
    prompt.present();
  }

}
