import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BhPage } from './bh';

@NgModule({
  declarations: [
    BhPage,
  ],
  imports: [
    IonicPageModule.forChild(BhPage),
  ],
  exports: [
    BhPage
  ]
})
export class BhPageModule {}
