import { Component } from '@angular/core';
import {Events, IonicPage, NavController} from 'ionic-angular';

import { PacienteProvider } from '../../providers/paciente/paciente.service';
import { Paciente } from "../../models/paciente/paciente.model";

import { PacienteDetailPage } from '../paciente_detail/paciente_detail';

@IonicPage()
@Component({
  selector: 'page-paciente-list',
  templateUrl: 'paciente_list.html',
})
export class PacienteListPage {

  pacientes: Paciente[] = [];

  constructor(private pacienteProvider: PacienteProvider, private events: Events, private navCtrl: NavController) {
    this.getAllPacientes();
    this.updatePacienteList();
  }

  private getAllPacientes(){
    this.pacienteProvider.getAllPacientes(true)
      .then(pacientes => {
        if(pacientes){
          this.pacientes = pacientes
        }
      })
  }

  private updatePacienteList(){
    this.events.subscribe("pacienteList:update", () => {
      this.getAllPacientes();
    });
  }

  newPaciente(){
    this.pacienteProvider.newPaciente();
  }

  openPaciente(pacienteId: number){
    this.pacienteProvider.openPaciente(pacienteId)
      .then(()=> {
        this.navCtrl.push(PacienteDetailPage);
      })
  }

}
