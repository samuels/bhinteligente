import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BalancoInitPage } from './balanco-init';

@NgModule({
  declarations: [
    BalancoInitPage,
  ],
  imports: [
    IonicPageModule.forChild(BalancoInitPage),
  ],
})
export class BalancoInitPageModule {}
