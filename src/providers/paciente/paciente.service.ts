import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import { StorageProvider } from '../storage/storage.service';
import { Paciente } from "../../models/paciente/paciente.model";
import { Events, ModalController } from "ionic-angular";

import { PacienteCreatePage } from '../../pages/paciente_create/paciente_create';

@Injectable()
export class PacienteProvider {

  public pacienteAberto: Paciente;

  constructor(private storageProvider: StorageProvider, private modalCtrl: ModalController, private events: Events) {}

  public getAllPacientes(reverse?: boolean): Promise<Paciente[]>{
    return this.storageProvider.getAllPacientes(reverse);
  }

  public newPaciente(){
    let modal = this.modalCtrl.create(PacienteCreatePage);
    modal.onDidDismiss(paciente => {
      if(paciente){
        this.storageProvider.createPaciente(paciente)
          .then( () => {
            this.publishEvent(["pacienteList:update"]);
          })
      }
    });
    modal.present();
  }

  public publishEvent(topics: string[]): void{
    topics.forEach(topic => {
      this.events.publish(topic)
    })
  };

  public getPaciente(pacienteId: number): Promise<Paciente>{
    return this.storageProvider.getPaciente(pacienteId);
  }

  public openPaciente(pacienteId: number): Promise<boolean>{
    return this.getPaciente(pacienteId)
      .then(paciente => {
        this.pacienteAberto = paciente;
        return true
      })
      .catch( () => {
        return false
      })
  }

}
