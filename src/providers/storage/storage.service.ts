import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { Paciente } from '../../models/paciente/paciente.model';

@Injectable()
export class StorageProvider {

  constructor(private storage: Storage) {}

  private save(paciente: Paciente): Promise<Paciente>{
    return this.storage.set(`paciente.${paciente.id}`, paciente)
  }

  public createPaciente(paciente: Paciente): Promise<Paciente>{
    return this.save(paciente)
  }

  public updatePaciente(paciente: Paciente): Promise<Paciente>{
    return this.save(paciente);
  }

  public getPaciente(pacienteId: number): Promise<Paciente>{
    return this.storage.get(`paciente.${pacienteId}`)
  }

  public deletePaciente(pacienteId: number): Promise<boolean>{
    return this.storage.remove(`paciente.${pacienteId}`);
  }

  public getAllPacientes(reverse?: boolean): Promise<Paciente[]>{
    return new Promise(resolve => {
      return this.storage.ready()
        .then( () => {

          let pacientes: Paciente[] = [];

          return this.storage.forEach((paciente: Paciente, key: string) => {
            if(key.indexOf('paciente.') > -1){
              pacientes.push(paciente);
            }
          })
            .then(() => resolve((!reverse) ? pacientes : pacientes.reverse()));
        })
    })
  }

}
