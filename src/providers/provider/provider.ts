import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import {Storage} from "@ionic/storage";

@Injectable()
export class Provider {


  constructor(private storage: Storage ) {}


  initBHD(){
    return this.storage.set('BHD', {'LA': [], 'LE': [], 'PI': {'peso': 0, 'horas': '00:00'}})
  }

  initCBH(){
    return this.storage.set('CBH', []);
  }


  get(local){
    return this.storage.get(local['data'])
      .then(data => {
        if(local['data'] == 'BHD'){
          return data[local['value']];
        }else if(local['data'] == 'CBH'){
          return data;
        }
      })
  }

  set(local, valor){
    return this.storage.get(local['data'])
      .then(
        data => {
          if(local['data'] == 'BHD'){
            if(local['value'] == 'PI'){
              data[local['value']] = valor;
            }else{
              data[local['value']].push(valor)
            }
          } else if(local['data'] == 'CBH'){
            data.push(valor)
          }
          return this.storage.set(local['data'], data)
        }
      )
  }

  total(local){
    let total = 0;
    return this.storage.get(local['data'])
      .then(
        data => {
          if(local['data'] == 'BHD'){
            for(let x=0; x < data[local['value']].length; x++){
              total += parseFloat(data[local['value']][x].volume)
            }
          }else if(local['data'] == 'CBH'){
            for(let x=0; x < data.length; x++){
              total += parseFloat(data[x].volume)
            }
          }
          return total
        }
      )
  }

  calcPI(){
    let total = 0;
    return this.get({'data': 'BHD', 'value': 'PI'})
      .then(data => {
        total = (parseFloat(data['peso']) * 0.5 * this.parseHorasNumber(data['horas']));
        return total
      })
  }

  parseHorasNumber(horas){
    let h = 0;
    let m = 0;
    if(horas){
      h = parseInt(horas.split(':')[0]);
      m = parseInt(horas.split(':')[1])/60;
      return h + m;
    }
    return 0;
  }

  bh(){
    let bh = 0;
    return Promise.all([
      this.total({'data': 'BHD', 'value': 'LA'}),
      this.total({'data': 'BHD', 'value': 'LE'}),
      this.calcPI()
    ])
      .then(([TLA, TLE, PI]) => {
        bh = TLA - (TLE + PI);
        return bh
      })
  }

  totalCBH(){
    let total = 0;
    return this.get({'data': 'CBH'})
      .then(data => {
        for(let x=0; x<data.length; x++){
          total += parseFloat(data[x]['value']);
        }
        return total;
      })
  }

  remove(local, index){
    return this.storage.get(local['data'])
      .then(data => {
        if(local['data'] == "BHD"){
          data[local['value']].splice(index, 1);
        }else if(local['data'] == 'CBH'){
         data.splice(index, 1)
        }
        return this.storage.set(local['data'], data)
      })
  }

}