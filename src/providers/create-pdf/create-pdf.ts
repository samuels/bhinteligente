import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

import * as pdfMake from 'pdfmake/build/pdfmake';
import * as pdfFonts from 'pdfmake/build/vfs_fonts';

import { EmailComposer } from '@ionic-native/email-composer';


@Injectable()
export class CreatePdfProvider {

  private docContent: any;
  private date = new Date().toISOString().slice(0, 10);

  constructor(private emailComposer: EmailComposer) {
    pdfMake.vfs = pdfFonts.pdfMake.vfs;
  }

  private addliquidos(title, data){
    this.docContent.content.push({text: title, style: 'subHeader'});
    let tablecontent = [];

    data.forEach(function (l) {
      tablecontent.push([l.liquido.verbose, {text: l.volume + ' mls'}])
    });

    this.docContent.content.push({
      table: {
        widths: ['*', '*' ],
        body: tablecontent
      },
      layout: 'noBorders'
    })
  }

  private listCBH(data){
    let tableContent = [[{text: 'Data', margin: [0, 0, 0, 10], bold: true}, {text: 'Balanço Hidrico', margin: [0, 0, 0, 10], bold: true}]];

    data.forEach(function (l) {
      tableContent.push([l.date, {text: l.value + ' mls'}])
    });

    this.docContent.content.push({
      table: {
        widths: ['*', '*' ],
        body: tableContent
      },
      layout: 'noBorders'
    })

  }

  private docDefinition(local, data){

    if(local == 'BHD'){
      this.docContent = {
        pageSize: 'A4',
        content: [
          {
            text: 'BHInteligente Relatório\n\n',
            style: 'header'
          },
          {text: 'Data: ' + this.date},
          {text: 'Nome do Paciente: ' + data['paciente']},
          {text: 'Peso: ' + data['peso'] + ' Kg'},
          {text: 'Numero de Horas Internado: ' + data['numHoras'] + ' h'},
          {text: 'Balanço Hidrico: ' + data['bh'] + ' mls'},
          {text: 'Total de Liquidos Administrados: ' + data['totalLA'] + 'mls'},
          {text: 'Total de Liquidos Eliminados: ' + data['totalLE'] + 'mls'},
          {text: 'Perda Insensível: ' + data['PI'] + ' mls'},

        ]
      };
      if(data['LA'].length > 0){
        this.addliquidos('Liquidos Administrados', data['LA']);
      }
      if (data['LE'].length > 0){
        this.addliquidos('Liquidos Eliminados', data['LE']);
      }

    }else if(local == 'CBH'){
      this.docContent = {
        pageSize: 'A4',
        content: [
          {
            text: 'BHInteligente Relatório\n\n',
            style: 'header'
          },
          {text: 'Data: ' + this.date},
          {text: 'Paciente: ' + data['paciente']},
          {text: 'Balanço Hidrico Consolidado: ' + data['CBHtotal'] + ' mls'},
          {text: 'Lista de Balanços', style: 'subHeader'}
        ]
      };
      if(data['CBHlist'].length > 0){
        this.listCBH(data['CBHlist']);
      }else{
        this.docContent.content.push({text: 'Vazia!'})
      }

    }

    this.docContent.styles = {
      header: {
        fontSize: 18,
        bold: true,
        alignment: 'center',
        margin: [0,0,0,80]
      },
      subHeader: {
        fontSize: 16,
        bold: true,
        alignment: 'left',
        margin: [0,50,0,20]
      }
    }

  }

  compartilhar(values){
    this.docDefinition(values['local'], values['data']);
    // pdfMake.createPdf(this.docContent).open();
    pdfMake.createPdf(this.docContent).getBase64((base64) => {
      this.emailComposer.open({
        attachments: ['base64:relatorio.pdf//' + base64],
        subject: 'BH Inteligente',
        body:    'Relatorio'
      })
    })
  }

}
