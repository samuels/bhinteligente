import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from "@ionic/storage";

import { BhInteligenteApp } from './app.component';

import { PacienteListPageModule } from '../pages/paciente_list/paciente_list.module';
import { PacienteCreatePageModule } from '../pages/paciente_create/paciente_create.module';
import { PacienteDetailPageModule } from '../pages/paciente_detail/paciente_detail.module';

import { HomePageModule } from '../pages/home/home.module';
import { LiquidosAdministradosModule } from '../pages/liquidos-administrados/liquidos-administrados.module';
import { LiquidosEliminadosModule } from '../pages/liquidos-eliminados/liquidos-eliminados.module';
import { BhPageModule } from '../pages/bh/bh.module';
import { PerdasInsesiveisPageModule } from '../pages/perdas-insesiveis/perdas-insesiveis.module';
import { BalancoDiaPageModule } from '../pages/balanco-dia/balanco-dia.module';
import { BalancoConsolidarPageModule } from '../pages/balanco-consolidar/balanco-consolidar.module';

import { Provider } from '../providers/provider/provider';
import { CreatePdfProvider } from '../providers/create-pdf/create-pdf';

import { EmailComposer } from '@ionic-native/email-composer';

import { StorageProvider } from '../providers/storage/storage.service';
import { PacienteProvider } from '../providers/paciente/paciente.service';

@NgModule({
  declarations: [
    BhInteligenteApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(BhInteligenteApp),
    IonicStorageModule.forRoot(),
    HomePageModule,
    LiquidosAdministradosModule,
    LiquidosEliminadosModule,
    BhPageModule,
    PerdasInsesiveisPageModule,
    BalancoDiaPageModule,
    BalancoConsolidarPageModule,
    PacienteListPageModule,
    PacienteCreatePageModule,
    PacienteDetailPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    BhInteligenteApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Provider,
    CreatePdfProvider,
    EmailComposer,
    StorageProvider,
    PacienteProvider
  ]
})
export class AppModule {}
