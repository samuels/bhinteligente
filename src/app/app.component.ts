import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { PacienteListPage } from "../pages/paciente_list/paciente_list";

import { Provider } from '../providers/provider/provider';

@Component({
  templateUrl: 'app.html'
})
export class BhInteligenteApp {

  // rootPage:any = HomePage;
  rootPage:any = PacienteListPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private provider: Provider) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.provider.initBHD();
      this.provider.initCBH();
    });
  }
}

